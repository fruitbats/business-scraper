/*
 * Queueing functions for the MongoDB based Queue
 *
 */

var mongoose = require('mongoose');
var Entry = require('./models').Entry;

module.exports.queue = function(params, callback){
    console.log('called queue');
    var newEntry = new Entry();
    newEntry.url = params.url;
    newEntry.name = params.name;
    newEntry.status = 'Not Processed';
    newEntry.save(function(err, newEntry){
        if (err){
            console.log(err);
            return callback(err);
        } 
        return callback(null);
    });
}


