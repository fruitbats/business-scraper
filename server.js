var config = require('./config');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var GooglePlaces = require('googleplaces');
var places = new GooglePlaces(config.PLACES_API_KEY, 'json');
var queue = require('./queue').queue;

mongoose.connect(config.MONGO_LOCAL);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/locations', function(req, res){
    console.log('New posted location');
    console.log(req.body);
    var parameters = {
        location: [req.body.lat, req.body.lng],
        types: req.body.types
    };
    places.placeSearch(req.body, function(err, res){
        if (err){
            console.log(err);
            res.status(400).send();
        } else{
           res.results.forEach(function(el, index, arr){
               var arg = {reference: el.reference};
               findAndQueue(arg);
           }); 
        }
    });
    res.status(200).send();
});

function findAndQueue(arg){
    places.placeDetailsRequest(arg, function(err, res){
        if (err){
            console.log(err);
        } else{
            console.log(res.result.name);
            var params = {url: res.result.website, name: res.result.name};
            queue(params, function(err){
                console.log('queueing');
                if (err){
                    console.log(err);
                    return err;
                } else {
                    console.log('success');
                }
            });
        }
    });
}


app.listen(8000, function(){
    console.log('Listening on port 8000...');
});
