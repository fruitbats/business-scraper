var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var entrySchema = new Schema({
    url: String,
    name: String,
    status: {
        type: String, 
        enum: ['Complete', 'Processing', 'Not Processed'], 
        default: 'Not Processed'
    }
});


var businessSchema = new Schema({
    name: String,
    email: String,
    url: String
});


module.exports.Entry = mongoose.model('Entry', entrySchema);
module.exports.Business = mongoose.model('Business', businessSchema);

